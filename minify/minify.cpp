﻿// minify.cpp : Defines the entry point for the application.
//

#include "minify.h"
#include <fstream>
#include <chrono>
#include "gdal.h"
#include "gdal_priv.h"

using namespace std;

enum class SamplingStrategy
{
	Random,	Accuracy
};

enum class BenefitsMethod {
	Compression,
	Accuracy
};

int GetSamplingSize(GDALDataset* input){
	//TODO: Come up with a better method to calculate sampling size
	//TODO: Figure out if dinamyc sampling is a good idea. Hint: probably it is, since distribution is unknown. If it is, this function is probably not useful
	int samplingSize = input->GetRasterXSize() * input->GetRasterYSize() / 100 / 100;
	samplingSize = samplingSize > 100 ? samplingSize : 100;
	samplingSize = 100; // the above line is a really bad idea, I mean, what if
	return samplingSize;
}

int* GetPatchSize(GDALDataset* input) {
	//TODO: Come up with a better method to calculate patch size
	return new int[2]{
		input->GetRasterXSize() / 100 < 26 ? 26 : input->GetRasterXSize() / 100,
		input->GetRasterYSize() / 100 < 26 ? 26 : input->GetRasterYSize() / 100, };
}

int(* GetSamplingLocations(GDALDataset* input, int sampleSize, int* patchSize, SamplingStrategy samplingStrategy, int* validationCoordinates = NULL))[2] {
	int(*randXY)[2] = new int[(size_t)sampleSize][2];
	switch (samplingStrategy)
	{
	case SamplingStrategy::Random:
		for (auto i = 0; i < sampleSize; i++) {
			randXY[i][0] = (rand() % ((input->GetRasterXSize() - patchSize[0])/patchSize[0]))*patchSize[0];
			randXY[i][1] = (rand() % ((input->GetRasterYSize() - patchSize[0])/patchSize[1]))*patchSize[1];
		}
		break;
	case SamplingStrategy::Accuracy:
		//TODO: Handle sampling when validation dataset exists
		break;
	}
	return randXY;
}

float GetBenefits(char* outputFilename, BenefitsMethod method) {
	float benefit;
	switch (method)
	{
	case BenefitsMethod::Compression:
	{
		char command[255];
		sprintf(command, "7z a output.7z %s -bso0 -bse0 -bsp0", outputFilename);
		system(command);
		std::ifstream in("output.7z", std::ifstream::ate | std::ifstream::binary);
		benefit = in.tellg();
		remove("output.7z");
		remove(outputFilename);
		break;
	}
	case BenefitsMethod::Accuracy:
		break;
	default:
		break;
	}
	return benefit;
}

int Sample(char* algorithm, char* inputFilename, char* outputFilename) {
	char command[255];
	sprintf(command, "%s %s %s", algorithm, inputFilename, outputFilename);
	return system(command);
}

int main(int argc, char* argv[])
{
	if (argc<3)
	{
		cout << "USAGE \n armin script.bat input.tif outputFilename.tif results.csv \nREQUIREMENTS\nGDAL";
		exit;
	}
	char* algorithm = argv[1];
	char* inputFilename = argv[2];
	char* outputFilename = argv[3];
	char* resultsFilename = argv[4];
	GDALAllRegister(); //this must be here just because GDAL
	
	// declare and initialize the important variables
	GDALDataset* input = (GDALDataset *) GDALOpen(inputFilename, GDALAccess::GA_ReadOnly);
	GDALDriver* poDriver = input->GetDriver();
	int nBands = input->GetRasterCount();
	int* patchSize = GetPatchSize(input);
	int const sampleSize = GetSamplingSize(input);
	int(*randXY)[2] = GetSamplingLocations(input, sampleSize, patchSize, SamplingStrategy::Random);
	// for (size_t i = 0; i < sampleSize; i++)
	// {
	// 	cout << randXY[i][0] << "," << randXY[i][1] << " ; ";
	// }
	int maxDownsampling = patchSize[0] < patchSize[1] ? patchSize[0] : patchSize[1];
	std::ofstream results;
	results.open(resultsFilename);
	results << "spatialDownsampling,temporalDownsampling,cost,benefit" << endl;
	cout << "Cost Benefit analysis on " << algorithm << " " << inputFilename << " " << outputFilename<<endl;
	cout << "Input file X size: " << input->GetRasterXSize()<<endl;
	cout << "Input file Y size: " << input->GetRasterYSize()<<endl;
	cout << "Input file  bands: " << nBands << endl;
	cout << "Sample size: " << sampleSize << endl;
	cout << "X patch size: " << patchSize[0] << endl;
	cout << "X patch size: " << patchSize[0] << endl;
	cout << endl;
	//GDALGetDataTypeSizeBytes
	GDALDataType dtype = input->GetRasterBand(1)->GetRasterDataType();
	//play time
	//maxDownsampling = 4;
	for (int spatialDownsampling = 1; spatialDownsampling < maxDownsampling / 2; spatialDownsampling++) {
		cout << spatialDownsampling << endl;

		int temporalDownsampling = 1;
		void* pData = CPLMalloc((size_t)GDALGetDataTypeSizeBytes(dtype) *				// pixel bytes
			patchSize[0] / spatialDownsampling * patchSize[1] / spatialDownsampling *	// area  bytes
			nBands / temporalDownsampling);												// bands bytes
		float totalBenefit = 0;
		int totalCost = 0;
		for (auto i = 0; i < sampleSize; i++)
		{
			// Read the input dataset
			// The Nearest Neighbour resampling is done as following:
			// for an odd spatialDownsampling, out of the 4 pixels in the center of the previous pixel, the bottom right is chosen
			// for an even spatialDownsampling, the center pixel is chosen
			// other resampling options can be specified as last parameter of RasterIO
			input->RasterIO(GF_Read, randXY[i][0], randXY[i][1], patchSize[0], patchSize[1],
				pData, patchSize[0] / spatialDownsampling, patchSize[1] / spatialDownsampling, dtype, nBands, NULL,
				0, 0, 0, NULL);

			// Create the patch dataset
			char patchFilename[255];
			sprintf(patchFilename, "patch_X%d_Y%d_S%d_T%d.tif", randXY[i][0], randXY[i][1], spatialDownsampling, temporalDownsampling);
			GDALDataset* patch = poDriver->Create(patchFilename, patchSize[0] / spatialDownsampling, patchSize[1] / spatialDownsampling, nBands, dtype, NULL);

			// Copy metadata over
			patch->SetMetadata(input->GetMetadata());
			patch->SetSpatialRef(input->GetSpatialRef());

			// Write the patch dataset
			patch->RasterIO(GF_Write, 0, 0, patchSize[0] / spatialDownsampling, patchSize[1] / spatialDownsampling, pData, patchSize[0] / spatialDownsampling, patchSize[1] / spatialDownsampling, dtype, nBands, NULL, 0, 0, 0, NULL);

			// Set spatial Extent
			double it[6];
			double ot[6];
			memcpy(ot, it, sizeof(it));

			input->GetGeoTransform(it);
			ot[0] = it[0] + randXY[i][0] * it[1] + randXY[i][1] * it[2];	// X Top Left
			ot[1] = it[1] * spatialDownsampling;							// X Scale
			ot[3] = it[3] + randXY[i][0] * it[4] + randXY[i][1] * it[5];	// Y Top Left
			ot[5] = it[5] * spatialDownsampling;							// Y Scale
			patch->SetGeoTransform(ot);

			// Close patch dataset
			GDALClose((GDALDatasetH)patch);

			// Sample
			auto t1 = std::chrono::high_resolution_clock::now();
			Sample(algorithm, patchFilename, outputFilename);
			auto t2 = std::chrono::high_resolution_clock::now();
			auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
			cout << "Cost:" << duration << endl;
			totalCost += duration;

			// Calculate costs and benefits for this patch
			// TODO: check if sampling output exists first
			float benefit = GetBenefits(outputFilename, BenefitsMethod::Compression);
			cout << "Benefit: " << benefit << endl;
			// TODO: think whether adding them up makes most sense, other options could be averaging
			totalBenefit += benefit;
		}
		results << spatialDownsampling << "," << temporalDownsampling << "," << totalCost << "," << totalBenefit << endl;
		CPLFree(pData);
	}
	GDALClose((GDALDatasetH)input);
	delete patchSize;
	delete randXY;
	results.close();
	return 0;
}