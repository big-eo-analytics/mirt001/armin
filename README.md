# armin

a remote sensing resolution minifier

it does [what any minifier does](https://en.wikipedia.org/wiki/Minification_(programming)),

loses the big, but keeps the data.

## install

Download windows binaries from [here](https://wageningenur4-my.sharepoint.com/:u:/g/personal/andrei_mirt_wur_nl/ER7qO5YiEtNMmlUsTCrn1s0BcHW8HuhYLaNCP5EX0cFfuw?e=lGY7iH).
Linux binaries should be available soon, but feel free to build the solution from source code.

## use

```
> armin script input.tif outputFilename.tif results.csv
```

script is expected to be an executable that accepts input.tif as its input and writes the output to outputFilename.tif

If your script expects different parameters, or a different order, or it is an interpretable script, you could write a short batch/bash script that calls the extra parameters, or reorders parameters, or calls the interpreter.

### Windows

```batch
@ECHO OFF
rscript rscript.R -prearguments %1 -nested %2 -any -other -arguments -go -here
```
### Linux

```bash
#!/bin/bash
rscript rscript.R -prearguments $1 -nested $2 -any -other -arguments -go -here
```

## develop

No instructions yet, but feel free to chip in.
